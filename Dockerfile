FROM frolvlad/alpine-oraclejdk8:slim
MAINTAINER Nicolás Rojas "nrojas@ebizlatin.com"
ENV REFRESHED_AT 2017-08-26 13:20
#VOLUME /tmp
#VOLUME /Apps/arca/ms-customer
ADD ./com.ebiz.msenvioemail.rest/target/com.ebiz.msenvioemail.rest-1.0-SNAPSHOT.jar app.jar
RUN sh -c 'touch /app.jar'
EXPOSE 8080
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java -Xmx32m -Xss256k $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]
