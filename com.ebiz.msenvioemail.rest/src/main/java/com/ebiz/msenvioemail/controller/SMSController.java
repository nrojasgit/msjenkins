package com.ebiz.msenvioemail.controller;

import com.ebiz.msenvioemail.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/email")
public class SMSController {

	@Autowired
	private EmailService emailService;

	@RequestMapping(value = "/send", method = RequestMethod.GET)
	public String send(@RequestParam(value = "recipient") String recipient, @RequestParam(value = "msj") String mensaje,
			@RequestParam(value = "subject") String subject) {

		return emailService.enviarMensaje(recipient, mensaje, subject);
	}

}
