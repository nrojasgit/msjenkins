package com.ebiz.msenvioemail;

public interface EmailService {
	
	String enviarMensaje(String recipient, String mensaje, String subject);

}
