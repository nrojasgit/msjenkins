package com.ebiz.msenvioemail.impl;

import com.ebiz.msenvioemail.EmailService;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import org.apache.commons.lang.exception.ExceptionUtils;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JavaMailSender sender;

    @Value("${spring.mail.username}")
    private String sendmail;

    public String enviarMensaje(String recipient, String mensaje, String subject) {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        try {
            helper.setTo(recipient);
            helper.setText(mensaje);
            helper.setSubject(subject);
            helper.setFrom(sendmail, "Ebiz Latin America");
            sender.send(message);

            return String.format("Mensaje enviado a: %s", recipient);
        } catch (Exception e) {
            return String.format("Error al enviar el mensaje<br><br>%s<br><br>%s", e.getMessage(), ExceptionUtils.getStackTrace(e));
        }
    }
}
